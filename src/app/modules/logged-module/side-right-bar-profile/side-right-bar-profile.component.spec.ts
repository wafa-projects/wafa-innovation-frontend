import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideRightBarProfileComponent } from './side-right-bar-profile.component';

describe('SideRightBarProfileComponent', () => {
  let component: SideRightBarProfileComponent;
  let fixture: ComponentFixture<SideRightBarProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideRightBarProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideRightBarProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
