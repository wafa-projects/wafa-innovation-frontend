import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { QuestionProvider } from 'src/app/shared/providers/questions';
import { DataService } from 'src/app/shared/providers/pageSharing';
import { AuthProvider } from 'src/app/shared/providers/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-questions-add',
  templateUrl: './questions-add.component.html',
  styleUrls: ['./questions-add.component.css']
})
export class QuestionsAddComponent implements OnInit {

  
  submitted = false;

  addForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private toaster: ToastrService,private router: Router,private authService:AuthProvider, private apiService: QuestionProvider) { }
 
  
  ngOnInit() {


    this.addForm = this.formBuilder.group({
     
        
        description: [null,{ validators: [Validators.required] }],
 
    });

  }


 



  onSubmit() {
    this.submitted = true;

   

    if (this.addForm.invalid) {
        return;
    }

   
    let question = {
      utilisateurId :  this.authService.retrieveLoggedUser().utilisateurId,
      description : this.addForm.value.description
    }
    this.apiService.postQuestion(question)
      .subscribe( data => {
        this.addForm.reset();
        this.toaster.success("Votre question a été envoyé avec seccuss ");
        
      },err=>{
        this.toaster.error( err.error.message);
      });

}

  
 

 

}
