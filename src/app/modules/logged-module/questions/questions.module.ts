import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionsComponent } from './questions-liste/questions.component';
import { QuestionsAddComponent } from './questions-add/questions-add.component';
import { QuillModule } from 'ngx-quill';
 
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 


@NgModule({
  declarations: [QuestionsComponent,
  QuestionsAddComponent,
  
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    QuillModule.forRoot(),
  ],  
  exports: [
    QuestionsComponent,
    QuestionsAddComponent],
})
export class QuestionsModule { }
