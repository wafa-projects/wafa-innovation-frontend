import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-last-sondage',
  templateUrl: './last-sondage.component.html',
  styleUrls: ['./last-sondage.component.css']
})
export class LastSondageComponent implements OnInit {

  sondages = [
    {
      title : "Solution digitale ",
      description : "Nous vous offrons à nos prospects une solution innovante qui leur permettra d'effectuer leur demande de crédit en ligne. Partagez avec nous votre avis ",
      datePublication: "02/17/2020 17:20:30",
    },

    {
      title : "Agence  ",
      description : "Vous vous êtes rendu dernièrement dans l'une de nos agences ou vous travaillez dans une agence, partagez avec nous votre avis sur l'expérience client. ",
      datePublication: "08/14/2010 17:20:30",
    }


  ]

  constructor() { }

  ngOnInit(): void {
  }

}
