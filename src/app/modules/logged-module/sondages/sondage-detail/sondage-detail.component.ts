import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sondage-detail',
  templateUrl: './sondage-detail.component.html',
  styleUrls: ['./sondage-detail.component.css']
})
export class SondageDetailComponent implements OnInit {
 
  sondage = {
    id:"11",
    title : "Solution digitale ",
    description : "Nous vous offrons à nos prospects une solution innovante qui leur permettra d'effectuer leur demande de crédit en ligne. Partagez avec nous votre avis ",
    datePublication: "02/17/2020 17:20:30",
    questions : [
      {
        questionId : 122,
        question : "Question 1",
        respones :[ {
          responseId : 1,
          response : "Oui"
        },
        {
          responseId : 2,
          response : "Non"
        }],
      },
      {
        questionId : 123,
        question : "Question 2",
        respones :[ {
          responseId : 1,
          response : "Oui"
        },
        {
          responseId : 2,
          response : "Non"
        }],
      },
      {
        questionId : 124,
        question : "Question 3",
        respones :[  ],
      },

    ]
  }


  constructor() { }

  ngOnInit(): void {
  }

}
