import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-logged-page',
  templateUrl: './logged-page.component.html',
  styleUrls: ['./logged-page.component.css']
})
export class LoggedPageComponent implements OnInit {


  selectedPage
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
     this.selectedPage = this.route.snapshot.paramMap.get("page")
  }

}
