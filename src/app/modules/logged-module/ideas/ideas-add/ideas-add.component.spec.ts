import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeasAddComponent } from './ideas-add.component';

describe('IdeasAddComponent', () => {
  let component: IdeasAddComponent;
  let fixture: ComponentFixture<IdeasAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdeasAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeasAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
