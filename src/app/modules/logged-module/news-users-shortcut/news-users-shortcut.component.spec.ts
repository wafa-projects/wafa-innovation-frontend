import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsUsersShortcutComponent } from './news-users-shortcut.component';

describe('NewsUsersShortcutComponent', () => {
  let component: NewsUsersShortcutComponent;
  let fixture: ComponentFixture<NewsUsersShortcutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsUsersShortcutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsUsersShortcutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
