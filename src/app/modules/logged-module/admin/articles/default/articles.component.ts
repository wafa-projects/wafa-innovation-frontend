import { Component, OnInit } from '@angular/core';
import { ApiProvider } from 'src/app/shared/providers/api.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  articles: any[] = [];
  constructor(
    private readonly api: ApiProvider
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  private getData() {
    this.api.getArticles(0, 10).subscribe(res => {
      this.articles = res['content'];
    });
  }

  onDelete(articleId) {
    if (confirm("Êtes-vous sûr de vouloir supprimer cet article ?")) {
        this.api.deleteArticle(articleId).subscribe(
          res=>{
            this.getData();
          }
        );
    }
  }

}
