import { Component, OnInit } from '@angular/core';
import { ApiProvider } from 'src/app/shared/providers/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-article-view',
  templateUrl: './article-view.component.html',
  styleUrls: ['./article-view.component.css']
})
export class ArticleViewComponent implements OnInit {


  article : any;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly api: ApiProvider,
  ) { }

  ngOnInit(): void {
    this.getId();
  }
  getId(): void {
    this.route.paramMap.subscribe(mapParams => {
      this.getData(mapParams['params'].id);
    });
  }

 getData(articleId){
  this.api.getArticleById(articleId).subscribe(
    res=>{
      this.article = res;
  });
 }

}
