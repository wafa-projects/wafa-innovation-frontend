import { Component, OnInit } from '@angular/core';
import { ApiProvider } from 'src/app/shared/providers/api.service';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {

  accounts: any[] = [];
  constructor(
    private readonly api: ApiProvider
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  private getData() {
    this.api.getUsers(0, 10).subscribe(res => {
      this.accounts = res['content'];
    });
  }

  onDelete(userId) {
    if (confirm("Êtes-vous sûr de vouloir supprimer cet utilisateur ?")) {
        this.api.deleteUser(userId).subscribe(
          res=>{
            this.getData();
          }
        );
    }
  }

}
