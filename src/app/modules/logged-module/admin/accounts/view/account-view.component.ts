import { Component, OnInit } from '@angular/core';
import { ApiProvider } from 'src/app/shared/providers/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-account-view',
  templateUrl: './account-view.component.html',
  styleUrls: ['./account-view.component.css']
})
export class AccountViewComponent implements OnInit {

  account : any;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly api: ApiProvider,
  ) { }

  ngOnInit(): void {
    this.getId();
  }
  getId(): void {
    this.route.paramMap.subscribe(mapParams => {
      this.getData(mapParams['params'].id);
    });
  }

 getData(id){
  this.api.getUserById(id).subscribe(
    res=>{
      this.account = res;
  });
 }

}
