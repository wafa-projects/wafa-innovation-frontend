import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-side-bar',
  templateUrl: './admin-side-bar.component.html',
  styleUrls: ['./admin-side-bar.component.css']
})
export class AdminSideBarComponent implements OnInit {

  sideBarItems = [
    {
        title : 'Comptes',
        icon: 'la la-lightbulb-o',
        link: '/administration/comptes'
    },
    {
      title : 'Idées',
      icon: 'la la-question-circle ',
      link: '/administration/idees'
  },
  {
    title : 'Articles',
    icon: 'la la-files-o ',
    link: '/administration/articles'
    
},
{
  title : "Enquêtes",
  icon: 'la la-area-chart  ',
  link: '/administration/enquetes'
  
},
{
  title : "Page d'acceuil",
  icon: 'la la-area-chart  ',
  link: '/administration/acceuil'
  
},


];
  constructor() { }

  ngOnInit(): void {
  }

}
