import { Component, OnInit } from '@angular/core';
import { SondageService } from 'src/app/shared/providers/sondages';

@Component({
  selector: 'app-sondages',
  templateUrl: './sondages.component.html',
  styleUrls: ['./sondages.component.css']
})
export class SondagesComponent implements OnInit {

  sondages 
  constructor(private api:SondageService) { }

  ngOnInit(): void {
    this.loadSondages()
  }
  
  
  private loadSondages() {
    this.api.loadSondages().subscribe(res => {
      this.sondages = res['content'];
    });
  }
}
