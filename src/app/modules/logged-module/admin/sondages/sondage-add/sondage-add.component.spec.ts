import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SondageAddComponent } from './sondage-add.component';

describe('SondageAddComponent', () => {
  let component: SondageAddComponent;
  let fixture: ComponentFixture<SondageAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SondageAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SondageAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
