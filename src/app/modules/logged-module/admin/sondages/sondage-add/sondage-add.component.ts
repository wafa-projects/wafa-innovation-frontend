import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-sondage-add',
  templateUrl: './sondage-add.component.html',
  styleUrls: ['./sondage-add.component.css']
})
export class SondageAddComponent implements OnInit {

 
form: FormGroup;
items: FormArray;

constructor(private formBuilder: FormBuilder) {}

ngOnInit() {
  this.form = this.formBuilder.group({
    titre: '',
    description: '',
    questionnaires : this.formBuilder.array([ this.createItem() ])
  });
}

createItem(): FormGroup {
  return this.formBuilder.group({
    description: '',
    price: ''
  });
}

}
