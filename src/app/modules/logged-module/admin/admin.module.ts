import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IdeasComponent } from './ideas/ideas.component';
import { SondagesComponent } from './sondages/sondages.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { AdminPageComponent } from './pages/admin-page/admin-page.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { AdminSideBarComponent } from './admin-side-bar/admin-side-bar.component';
import { SondageAddComponent } from './sondages/sondage-add/sondage-add.component';
import { AccountsComponent } from './accounts/default/accounts.component';
import { AccountViewComponent } from './accounts/view/account-view.component';
 


@NgModule({
  declarations: [
 
    AccountsComponent, 
    AccountViewComponent,
    IdeasComponent, 
    SondagesComponent, 
    SideBarComponent, 
    AdminPageComponent, AdminSideBarComponent, SondageAddComponent],
  imports: [
    CommonModule,

    RouterModule,
    BrowserModule
 
  ],
  exports :[
    AccountViewComponent,

    AccountsComponent, 
    IdeasComponent, 
    SondagesComponent, 
    SideBarComponent, 
    AdminPageComponent,
    SondageAddComponent
    
  
  ],
   providers: [],
})
export class AdminModule { }
