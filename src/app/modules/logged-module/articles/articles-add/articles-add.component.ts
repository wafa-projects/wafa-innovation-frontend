import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Router } from '@angular/router';
import { ArticlesProvider } from 'src/app/shared/providers/articles';
import { DataService } from 'src/app/shared/providers/pageSharing';
import { AuthProvider } from 'src/app/shared/providers/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-articles-add',
  templateUrl: './articles-add.component.html',
  styleUrls: ['./articles-add.component.css']
})
export class ArticlesAddComponent implements OnInit {

  submitted = false;
  addForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private toasterService:ToastrService, private router: Router,private authService: AuthProvider, private articleService: ArticlesProvider) { }

  modules = {
 
    toolbar: [      
      [{ header: [1, 2, false] }],
      [{ 'color': [] }, { 'background': [] }], 
      [{ 'align': [] }],
      ['bold', 'italic', 'underline'],
  
      ['blockquote', 'code-block','link', 'image', 'video'],
     
      ['clean'],  
    ],
  
  };

  
  
  

  

  
 
 
  
  ngOnInit() {
    
    this.addForm = this.formBuilder.group({
     
        article: [null,{ validators: [Validators.required] }],
        description: [null,{ validators: [Validators.required] }],
 
    });

  }


 

 



  onSubmit() {
    this.submitted = true;

     

    if (this.addForm.invalid) {
        return;
    }
 

     
    let article = {
      utilisateurId :  this.authService.retrieveLoggedUser().utilisateurId,
      description : this.addForm.value.description,
      isVisible:false
    
    }
    this.articleService.postArticle(article)
      .subscribe( data => {
        this.addForm.reset()
        this.toasterService.success("Votre article a été envoyé avec seccuss ");
      },err=>{
        this.toasterService.error( err.error.message);
      });

  }



}
