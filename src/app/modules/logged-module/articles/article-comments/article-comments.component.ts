import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AccountsService } from 'src/app/shared/providers/accounts.service';
import { AuthProvider } from 'src/app/shared/providers/auth.service';
import { ArticlesProvider } from 'src/app/shared/providers/articles';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-article-comments',
  templateUrl: './article-comments.component.html',
  styleUrls: ['./article-comments.component.css']
})
export class ArticleCommentsComponent implements OnInit {


  @Input()
  article

  loggedUser;

  form

  submitted = false;
  comments = []
 
  constructor(private toasterService:ToastrService,private formBuilder:FormBuilder,private accountService:AccountsService, private authService: AuthProvider, private articleService: ArticlesProvider) { }

  

  ngOnInit(): void {
    this.initForm();
    this.loadComments()
 }



 initForm(){


    this.form = this.formBuilder.group({
      
      comment: [null,{ validators: [Validators.required] }],
 

  });

 }

 loadComments(){
   
  this.loggedUser = this.authService.retrieveLoggedUser()
 
  this.articleService.loadCommentsOfArticle(this.article).subscribe(data =>{
     
    this.comments = data["content"]
  
  },error=>{
   // this.toasterService.error(error.error.massage)
  });

 }


 postComment(){

  this.submitted = true;

      

  if (this.form.invalid) {
      return;
  }

    let commentaire = {
      articleId : this.article,
      utilisateurId :  this.authService.retrieveLoggedUser().utilisateurId,
      description : this.form.value.comment,

    }


    this.loggedUser = this.authService.retrieveLoggedUser()
  
    this.articleService.commentArticle(commentaire).subscribe(data =>{
        this.loadComments()
    },error=>{
     // this.toasterService.error(error.error.massage)
    });
   


}

}
