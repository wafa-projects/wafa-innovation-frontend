import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/shared/providers/pageSharing';
import { ArticlesProvider } from 'src/app/shared/providers/articles';
import { AuthProvider } from 'src/app/shared/providers/auth.service';
import { ToastrService } from 'ngx-toastr';
import { AccountsService } from 'src/app/shared/providers/accounts.service';

@Component({
  selector: 'app-articles-liste',
  templateUrl: './articles-liste.component.html',
  styleUrls: ['./articles-liste.component.css']
})
export class ArticlesListeComponent implements OnInit {
  isLoading = true;
 loggedUser = {}
 articles = []

  constructor(private toasterService:ToastrService,private accountService:AccountsService, private authService: AuthProvider, private articleService: ArticlesProvider) { }

  

  ngOnInit(): void {
    this.loadArticles()
 }

 loadArticles(){
  let that = this
  
  this.loggedUser = this.authService.retrieveLoggedUser()
 
  this.articleService.loadArticles().subscribe(data =>{
    this.articles = data["content"];
    this.articles= this.articles.reverse();
    
    data["content"].forEach(function (item,index) {
      
      
      that.accountService.getUserById(item['utilisateurId']).subscribe(user =>{
         
        if(user['photoProfile']==null)
          user['photoProfile']="assets/images/resources/user1.png"

          that.articles[index]['user'] = user
          
      })
      debugger
      that.articleService.retrieveLikes(item['articleId']).subscribe(likes =>{
        
        that.articles[index]['likedByMe'] = false
          likes["content"].forEach(function (like) {
              if(like["utilisateurId"]==that.loggedUser["utilisateurId"])
                that.articles[index]['likedByMe'] = true
          })
        
         that.articles[index]['commentsShowd'] = false
         that.articles[index]['countLikes'] = likes['totalElements']
  
      })

    
      




    });
  
    this.isLoading = false
  


  
  },error=>{
     
  });

 }

  
 likeArticle(idArticle){
 

  this.loggedUser = this.authService.retrieveLoggedUser()
 
  let like = {
    articleId: idArticle,
    utilisateurId: this.loggedUser['utilisateurId']
  }
  this.articleService.likeArticle(like).subscribe(data =>{
   
  
  },error=>{
    //this.toasterService.error(error.error.massage)
  });

 }
 
 

}
