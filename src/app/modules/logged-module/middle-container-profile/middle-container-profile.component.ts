import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/shared/providers/pageSharing';

@Component({
  selector: 'app-middle-container-profile',
  templateUrl: './middle-container-profile.component.html',
  styleUrls: ['./middle-container-profile.component.css']
})
export class MiddleContainerProfileComponent implements OnInit {
  
   
  activeComponenet = "enquetes"
  constructor(private pageCurrentService:DataService) { }

  ngOnInit(): void {
    
    this.pageCurrentService.currentPage.subscribe(message => this.activeComponenet = message)
    console.log(this.activeComponenet )
  }

   
}
