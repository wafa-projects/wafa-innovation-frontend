import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/shared/providers/pageSharing';
import { AuthProvider } from 'src/app/shared/providers/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input()
  isLogged
  menuHidden = true;
  collapsed =false;
  loggedUser
  user = {
    id: "151654",
    email : "aboudrar.hassan.91@gmail.com",
    name : 'Hassan ABOUDRAR'
    
  }
  menu = [
    {
      title : 'Accueil',
      icon: 'la la-home',
      link: '/accueil',
      requireLogin : true ,
      requiredPermission : ["USER"]
  },

    {
      title : 'Mes articles',
      icon: 'la la-files-o ',
      link: '/mon-espace/articles',
      requireLogin : true ,
      requiredPermission : ["USER"]
      
  },
    {
      title : 'Mes questions',
      icon: 'la la-question-circle ',
      link: '/mon-espace/questions',
      requireLogin : true ,
      requiredPermission : ["USER"]
  },

  {
    title : 'Mes idees',
    icon: 'la la-lightbulb-o',
    link: '/mon-espace/idees',
    requireLogin : true,
    requiredPermission : ["USER"]
},

{
  title : 'Enquêtes',
  icon: 'la la-area-chart  ',
  link: '/mon-espace/enquetes',
  requireLogin : true ,
  requiredPermission : ["USER"]
},
{
  title : 'Administration',
  icon: 'la la-cogs  ',
  link: '/administration',
  requireLogin :  true,
  requiredPermission : ["ADMIN"]
  
}
];

  constructor(private serviceAuth:AuthProvider) { }

  ngOnInit() {
    this.loggedUser = this.serviceAuth.retrieveLoggedUser
    this.isLogged = this.loggedUser.role || false
    

  }
  
 

  

  


}
