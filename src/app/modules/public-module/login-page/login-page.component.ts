import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  currentPage = 'signin'
  quoteHome = "La créativité est contagieuse, faites la tourner. "

 
  constructor() { }

  ngOnInit(): void {
  }

}
