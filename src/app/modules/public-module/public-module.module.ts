import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';
  
import { LoginPageComponent } from './login-page/login-page.component';
import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConfirmationPageComponent } from './confirmation-page/confirmation-page.component';




@NgModule({
  declarations: [LoginPageComponent,HomePageComponent,SignupComponent, SigninComponent,ConfirmationPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
 

   
  ],
  exports: [LoginPageComponent,HomePageComponent,SignupComponent, SigninComponent, RouterModule,ConfirmationPageComponent
  ],
})
export class PublicModuleModule { }
