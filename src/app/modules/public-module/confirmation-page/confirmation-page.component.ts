import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiProvider } from 'src/app/shared/providers/api.service';

@Component({
  selector: 'app-confirmation-page',
  templateUrl: './confirmation-page.component.html',
  styleUrls: ['./confirmation-page.component.css']
})
export class ConfirmationPageComponent implements OnInit {

  userId: number = null;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router : Router,
    private readonly api: ApiProvider
  ) { }

  ngOnInit(): void {
    this.getId();
  }
  getId(): void {
    this.route.paramMap.subscribe(mapParams => {
      this.userId = mapParams['params'].id;
    });
  }

  onSubmit() {
    this.api.validateUser(this.userId).subscribe(
      res => {
        this.router.navigateByUrl("utilisateur/authentification");
      });
  }

}
