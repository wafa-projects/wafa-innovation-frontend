import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AccountsService } from 'src/app/shared/providers/accounts.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { AuthProvider } from 'src/app/shared/providers/auth.service';

@Component({
  selector: 'app-digi-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {


  registerForm: FormGroup;
  submitted = false;


  constructor(private formBuilder: FormBuilder, private authService: AuthProvider, private router: Router, private authentficationService: AccountsService, private toaster: ToastrService) {

    this.registerForm = this.formBuilder.group({

      email: [null, { validators: [Validators.required, Validators.email] }],
      password: [null, Validators.required],

    });
  }


  ngOnInit() {

  }


  get f() { return this.registerForm.controls; }

  onSubmit() {
    if (this.registerForm.valid) {
      this.authentficationService.login(this.registerForm.value).subscribe(res => {
        if (res['statut'] === "valide") {
          this.authService.signIn(res)
          this.router.navigate(['/mon-espace/articles']);
        } else {
          this.toaster.error("Veuillez confirmer votre inscription");
        }
      }, err => {
        this.toaster.error(err.error.message);
      });
    }
    this.submitted = true;

  }


}
