import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { stringify } from 'querystring';
 


@Injectable()
export class AuthProvider {

  private loggedIn: boolean;

  constructor(
    private readonly router: Router
  ) {
    this.checkAuthentication();
  }

  isLoggedIn(): boolean {
    return this.loggedIn;
  }

  signIn(token): void {
    localStorage.setItem(environment.TOKEN_NAME, btoa(JSON.stringify(token)));
    this.loggedIn = true;
  }

  signOut(): void {
    localStorage.removeItem(environment.TOKEN_NAME);
    this.loggedIn = false;
    this.router.navigate(['/utilisateur/authentification']);
  }

  checkAuthentication(): void {
    const token = this.retrieveLoggedUser();
    token ? this.signIn(token) : this.signOut();
  }

  retrieveLoggedUser() {
    const user =  localStorage.getItem(environment.TOKEN_NAME);
    if(!user)
    return user
    return JSON.parse(atob(user));
  }

}
