import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
 

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'multipart/form-data'
  })
};

@Injectable()
export class ApiProvider {


  baseUrl = environment.url +"/api";
 
  constructor(
    private readonly http: HttpClient,
  ) { }

  login(body: any) {
    return this.http.post(this.baseUrl + '/utilisateurs/auth', body);
  }

  getUserById(id) {
    return this.http.get(this.baseUrl + '/utilisateurs/'+id);
  }


  signup(body: any) {
    return this.http.post(this.baseUrl + '/utilisateurs', body);
  }

  getAllArticles() {
    return this.http.get(this.baseUrl + '/articles');
  }

  postArticle(body: any) {
    return this.http.post(this.baseUrl + '/articles', body);
  }

  updateArticle(body: any) {
    return this.http.put(this.baseUrl + '/articles', body);
  }

  
  getAllQuestions() {
    return this.http.get(this.baseUrl + '/question');
  }

  postQuestion(body: any) {
    return this.http.post(this.baseUrl + '/question', body);
  }

  getQuestionByUser(id:any,criteria:any) {
    return this.http.get(this.baseUrl + '/question/user/'+id);
  }


  getComments(articleId) {
    return this.http.get(this.baseUrl + '/articles/'+articleId+'/comments');
  }

  postComment(body: any) {
    return this.http.post(this.baseUrl + '/commentaires', body);
  }


  getIdeas() {
    return this.http.get(this.baseUrl + '/idees/');
  }

  getIdeesByUser(userId) {
    return this.http.get(this.baseUrl + '/utilisateur/'+userId+'/idees');
  }
  

  likeArticle(body: any) {
    return this.http.post(this.baseUrl + '/likes', body);
  }

  countLikesOfArticle(idArticle) {
    return this.http.get(this.baseUrl + '/nbrLikes/'+idArticle);
  }

  retrieveLikes(idArticle) {
    return this.http.get(this.baseUrl + '/likes/article/'+idArticle);
  }

  loadCommentsOfArticle(idArticle) {
    return this.http.get(this.baseUrl + '/commentaires/article/'+idArticle);
  }


  postCommentArticle(comment) {
    return this.http.post(this.baseUrl + '/commentaires/',comment);
  }


  loadSondages() {
    return this.http.get(this.baseUrl + '/enquete');
  }



  validateUser(userId: number) {
    return this.http.put(this.baseUrl + '/utilisateurs/valider/' + userId, null);
  }

  getUsers(page: number, size: number) {
    return this.http.get(this.baseUrl + '/utilisateur/?page=' + page + '&size=' + size);
  }

  deleteUser(userId: number) {
    return this.http.delete(this.baseUrl + '/utilisateurs/' + userId);
  }

}
