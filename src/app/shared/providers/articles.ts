 
import { Injectable } from '@angular/core';
 
 
import { ApiProvider } from './api.service';

@Injectable()
export class ArticlesProvider {

  constructor(
    private readonly api: ApiProvider
  ) {}

  loadArticles() {
    return this.api.getAllArticles()
  }

  likeArticle(like) {
    return this.api.likeArticle(like)
  }

  retrieveLikes(idArticle){
    return this.api.retrieveLikes(idArticle)
  }
  countLikeByArticle(idArticle) {
   return this.api.countLikesOfArticle(idArticle) 
  }

  postArticle(article) {
    return this.api.postArticle(article);
  }

  loadCommentsOfArticle(idArticle) {
    return this.api.loadCommentsOfArticle(idArticle)
  }

  commentArticle(comment){
    return this.api.postCommentArticle(comment)
  }



}
