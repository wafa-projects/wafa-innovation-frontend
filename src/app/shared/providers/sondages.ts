import { Injectable } from '@angular/core';
import { ApiProvider } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class SondageService {

  constructor(
    private readonly api: ApiProvider
  ) {}

  loadSondages() {
    return this.api.loadSondages();
  }

  
}
