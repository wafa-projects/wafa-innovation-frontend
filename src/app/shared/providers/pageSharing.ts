import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

  private page = new BehaviorSubject('enquetes');
  currentPage= this.page.asObservable();

  constructor() { }

  changeCurrentPage(pageChange: string) {
    this.page.next(pageChange)
  }

}