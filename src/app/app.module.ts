import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { ModulesModule } from './modules/modules.module';
import { RouterModule, Routes } from '@angular/router';

import { LoginPageComponent } from './modules/public-module/login-page/login-page.component';

import { ArticlesPageComponent } from './modules/logged-module/pages/articles-page/articles-page.component';
import { IdeasPageComponent } from './modules/logged-module/pages/ideas-page/ideas-page.component';
import { QuestionsPageComponent } from './modules/logged-module/pages/questions-page/questions-page.component';
import { SondagesPageComponent } from './modules/logged-module/pages/sondages-page/sondages-page.component';
import { ArticlesAddComponent } from './modules/logged-module/articles/articles-add/articles-add.component';
import { IdeasAddComponent } from './modules/logged-module/ideas/ideas-add/ideas-add.component';
import { QuestionsAddComponent } from './modules/logged-module/questions/questions-add/questions-add.component';
import { ArticlesListeComponent } from './modules/logged-module/articles/articles-liste/articles-liste.component';
import { QuestionsComponent } from './modules/logged-module/questions/questions-liste/questions.component';
import { RootComponent } from './modules/root/root.component';
import { SignupComponent } from './modules/public-module/signup/signup.component';
import { SigninComponent } from './modules/public-module/signin/signin.component';
import { SondagesListeComponent } from './modules/logged-module/sondages/sondages-liste/sondages-liste.component';
import { SondageDetailComponent } from './modules/logged-module/sondages/sondage-detail/sondage-detail.component';
import { AdminPageComponent } from './modules/logged-module/admin/pages/admin-page/admin-page.component';
 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



import { ToastrModule } from 'ngx-toastr';
import { AuthGuardService } from './shared/providers/auth-guards.service'; ;
import { ConfirmationPageComponent } from './modules/public-module/confirmation-page/confirmation-page.component';
import { SondagesComponent } from './modules/logged-module/admin/sondages/sondages.component';
import { SondageAddComponent } from './modules/logged-module/admin/sondages/sondage-add/sondage-add.component';
import { AccountsComponent } from './modules/logged-module/admin/accounts/default/accounts.component';
import { AccountViewComponent } from './modules/logged-module/admin/accounts/view/account-view.component';
import { HomePageComponent } from './modules/public-module/home-page/home-page.component';
const routes: Routes = [


 

  {
    path: 'confirmation/:id/:token',
    component: ConfirmationPageComponent,
  },

  {
    path: '', component: RootComponent,
    children: [
      {  path: '',  redirectTo : 'acceuil',pathMatch: 'full' },
      { path: 'acceuil', component: HomePageComponent
      },
    ]
  },



  {
    path: 'administration', component: RootComponent,
    canActivate: [AuthGuardService],
    children: [
 
      { path: '', redirectTo: 'comptes', pathMatch: 'full' },
      {
        path: 'comptes', component: AdminPageComponent,
        
        children: [
          { path: '', component: AccountsComponent },
          { path: 'consulter/:id', component: AccountViewComponent },

        ]
      },
      {
        path: 'enquetes', component: AdminPageComponent,
        
        children: [
          { path: '', component: SondagesComponent },
          { path: 'nouveau', component: SondageAddComponent },
        ]
      }
    ]
  },




  {
    path: 'utilisateur', component: LoginPageComponent,
    children: [

      { path: '', redirectTo: 'authentification', pathMatch: 'full' },
      { path: 'inscription', component: SignupComponent },
      { path: 'authentification', component: SigninComponent },

    ]
  },

  {
    path: 'mon-espace', component: RootComponent,

    children: [
      { path: '', redirectTo: 'articles', pathMatch: 'full' },
      {
        path: 'articles', component: ArticlesPageComponent,
        children: [
          { path: '', component: ArticlesListeComponent },
          { path: 'nouveau', component: ArticlesAddComponent },

        ]
      },
      {
        path: 'idees', component: IdeasPageComponent,
        children: [
          { path: '', component: IdeasAddComponent },
          { path: 'nouveau', component: IdeasAddComponent },

        ]
      },
      {
        path: 'questions', component: QuestionsPageComponent,
        children: [
          { path: '', component: QuestionsComponent },
          { path: 'nouveau', component: QuestionsAddComponent },

        ]
      },
      {
        path: 'enquetes', component: SondagesPageComponent,
        children: [
          { path: '', component: SondagesListeComponent },
          { path: ':id/detail', component: SondageDetailComponent },

        ]

      }]

  }




];


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    RouterModule.forRoot(routes),
    ModulesModule,

    ToastrModule.forRoot()

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
